/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var googleapi = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },

    authorize: function(options) {
        var deferred = $.Deferred();
        var authUrl = 'https://accounts.google.com/o/oauth2/auth?' + $.param({
            client_id: options.client_id,
            redirect_uri: options.redirect_uri,
            response_type: 'code',
            scope: options.scope
        });
        var authWindow = window.open(authUrl, '_blank', 'location=no,toolbar=no');
        return deferred.promise();
    },

    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'googleapi.receivedEvent(...);'
    onDeviceReady: function() {
        googleapi.receivedEvent('deviceready');
        googleapi.deviceIsReady();
    },

    deviceIsReady: function() {
        var $loginButton = $('#login a');
        var $loginStatus = $('#login p');

        $loginButton.on('click', function() {
            googleapi.authorize({
              client_id: '84584817434-oun0ih2aikq29al8u09m096qn3nabfg5.apps.googleusercontent.com',
              client_secret: 'ChnRG9wsdY4ki2Ss949k5LQu',
              redirect_uri: 'https://fackend-slowmo-223.appspot.com/',
              scope: 'https://www.googleapis.com/auth/analytics.readonly'
            }).done(function(data) {
              $loginStatus.html('Access Token: ' + data.access_token);
            }).fail(function(data) {
              $loginStatus.html(data.error);
            });
        });
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};
